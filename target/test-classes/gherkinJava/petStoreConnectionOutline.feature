Feature: Petstore conection 
 
   Scenario Outline: PetStore Connection
    	Given I am in petStore HomePage
			When I sign in with login <login> and password <password>
			Then The welcome page is displayed
			And The user <user> is connected 

			Examples:
			| login | password | user |
			| "j2ee" | "j2ee" | "ABC" |
			| "ACID" | "ACID" | "ABC" |